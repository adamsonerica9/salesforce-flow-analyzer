import json
from venv import create
import xml.etree.ElementTree as ET
import xml

import yaml

from sal_119 import (FLOW_LAYOUT, INCREMENT_X, INCREMENT_Y, create_tag,
                     namespace, save, xml_printer)
from screen_builder import build_flow

ET.register_namespace("", "http://soap.sforce.com/2006/04/metadata")


if __name__ == "__main__":

    recipe = yaml.safe_load(open("input/helpdesk_send_email_subflow_recipe.yaml", 'r'))
    # import json as js
    # print(js.dumps(recipe, indent=2))

    if 'recordCreates' in recipe.keys():
        # Preprocess recordCreates fields
        for recordCreate in recipe['recordCreates']:
            for inputAssignments in recordCreate['inputAssignments']:
                if 'elementReference' in inputAssignments.keys():
                    is_user_input_field = 'Field_' in inputAssignments['elementReference']

                    if is_user_input_field:
                        inputAssignments['elementReference'] = '_'.join([inputAssignments['elementReference'], 'when', recordCreate['name'].replace('CreateCase_when_', '')])
                        inputAssignments['elementReference'] += '.recordId' if 'isIDField' in inputAssignments.keys() and inputAssignments['isIDField'] == True else ''
                        inputAssignments['elementReference'] += '.value' if 'isEmailField' in inputAssignments.keys() and inputAssignments['isEmailField'] == True else ''

    if 'screens' in recipe.keys():
        # Preprocess Finalization screens fields
        for screen in recipe['screens']:
            is_final_screen = 'FinalizationScreen_' in screen['name']
            is_createcase_screen = 'CaseDetails_when_' in screen['name']

            # if is_createcase_screen:
            #     for field in screen['fields']:
            #         field['name'] = '_'.join([field['name'], 'when', screen['name'].replace('CaseDetails_when_', '')])
            
            if is_final_screen:
                for field in screen['fields']:
                    field['name'] = '_'.join([field['name'], 'when', screen['name'].replace('FinalizationScreen_when_', '')])
        

    start_locationX = recipe['start.locationX']
    start_locationY = recipe['start.locationY']

    flow = create_tag(element_type='base_skeleton', data={
        'label': recipe['label'],
        'start.locationX': str(recipe['start.locationX']),
        'start.locationY': str(recipe['start.locationY']),
        'start.targetReference': recipe['start.targetReference'],
        'description': recipe['description'],
    })
    tree = ET.ElementTree(flow)
    root = tree.getroot()
    ns = namespace(root)
    # xml_printer(flow)
    # print()

    for i in range(len(recipe['decisions'])):
        decisions = create_tag(element_type='decisions', data={
            'label': recipe['decisions'][i]['label'],
            'name': recipe['decisions'][i]['name'],
            'rules': [
                create_tag(element_type='decision_rules', data=
                    {
                        'name': rule['name'],
                        'label': rule['label'],
                        'conditionLogic': rule['conditionLogic'],
                        'conditions': rule['conditions'] if 'conditions' in rule.keys() else None,
                        'targetReference_c': rule['targetReference'] if 'targetReference' in rule.keys() else None
                    }
                    if 'conditions' in rule.keys() else 

                    {
                        'name': rule['name'],
                        'label': rule['label'],
                        'conditionLogic': rule['conditionLogic'],
                        'leftValueReference': rule['leftValueReference'],
                        'operator': rule['operator'],
                        'stringValue': rule['rightStringValue'],
                        'targetReference_c': rule['targetReference'] if 'targetReference' in rule.keys() else None
                    }
                )
                for rule in recipe['decisions'][i]['rules']
            ],
            'defaultConnectorLabel': recipe['decisions'][i]['defaultConnectorLabel'],
            'locationX': str(start_locationX),
            'locationY': str(start_locationY+INCREMENT_Y),
        })
        FLOW_LAYOUT['decisions'].append(decisions)

    if 'screens' in recipe.keys():
        case_details_screens = []
        for i in range(len(recipe['screens'])):
            fields = []
            for field in recipe['screens'][i]['fields']:
                data = {
                    "name": '_'.join([field['name'], 'when', recipe['screens'][i]['name'].replace('CaseDetails_when_', '')]) if 'bindToScreen' in field.keys() and field['bindToScreen'] == True else field['name'],
                    "dataType": field['dataType'],
                    "fieldText": field['fieldText'],
                    "fieldType": field['fieldType'],
                    "isRequired": field['isRequired'],
                } if field['fieldType'] == 'InputField' and 'choiceReferences' not in field.keys() else (

                    {
                        "name": '_'.join([field['name'], 'when', recipe['screens'][i]['name'].replace('CaseDetails_when_', '')]) if 'bindToScreen' in field.keys() and field['bindToScreen'] == True else field['name'],
                        "fieldText": field['fieldText'],
                        "fieldType": field['fieldType'],
                        "isRequired": field['isRequired'],
                    }
                    if field['fieldType'] == "LargeTextArea" else

                    {
                        "name": '_'.join([field['name'], 'when', recipe['screens'][i]['name'].replace('CaseDetails_when_', '')]) if 'bindToScreen' in field.keys() and field['bindToScreen'] == True else field['name'],
                        "dataType": field['dataType'],
                        "fieldText": field['fieldText'],
                        "fieldType": field['fieldType'],
                        "isRequired": field['isRequired'],
                        "choiceReferences": field['choiceReferences']
                    }
                    if field['fieldType'] in ['MultiSelectPicklist', 'DropdownBox'] and 'choiceReferences' in field.keys() else
                    
                    {
                        'name': '_'.join([field['name'], 'when', recipe['screens'][i]['name'].replace('CaseDetails_when_', '')]) if 'bindToScreen' in field.keys() and field['bindToScreen'] == True else field['name'],
                        'extensionName': field['extensionName'],
                        'fieldType': field['fieldType'],
                        'inputsOnNextNavToAssocScrn': field['inputsOnNextNavToAssocScrn'],
                        'storeOutputAutomatically': field['storeOutputAutomatically'],
                        'isRequired': field['isRequired'],
                        'inputParameters': field['inputParameters'],
                    }
                    if 'extensionName' in field.keys() and field['extensionName'] == 'flowruntime:lookup' else
                    
                    {
                        "name": field['name'],
                        "fieldType": field['fieldType'],
                        "isRequired": field['isRequired'],
                        "extensionName": field['extensionName'],
                        "inputsOnNextNavToAssocScrn": field['inputsOnNextNavToAssocScrn'],
                        "storeOutputAutomatically": field['storeOutputAutomatically'],
                        "inputParameters": field['inputParameters'],
                    }
                    if 'extensionName' in field.keys() and field['extensionName'] == 'forceContent:fileUpload' else

                    {
                        "name": '_'.join([field['name'], 'when', recipe['screens'][i]['name'].replace('CaseDetails_when_', '')]) if 'bindToScreen' in field.keys() and field['bindToScreen'] == True else field['name'],
                        "fieldType": field['fieldType'],
                        "isRequired": field['isRequired'],
                        "extensionName": field['extensionName'],
                        "inputsOnNextNavToAssocScrn": field['inputsOnNextNavToAssocScrn'],
                        "storeOutputAutomatically": field['storeOutputAutomatically'],
                        "inputParameters": field['inputParameters'],
                    }
                    if 'extensionName' in field.keys() and field['extensionName'] == 'flowruntime:email' else

                    {
                        'name': field['name'],
                        'fieldType': field['fieldType'],
                        'fieldText': field['fieldText'],
                    }
                    if field['fieldType'] == 'DisplayText' else None
                )

                if data is None:
                    print(field)
                    print()

                fields.append(create_tag(element_type='screen_field', data=data))
            
            screen_data = {
                'name': recipe['screens'][i]['name'],
                'label': recipe['screens'][i]['label'],
                'allowBack': recipe['screens'][i]['allowBack'],
                'allowFinish': recipe['screens'][i]['allowFinish'],
                'allowPause': recipe['screens'][i]['allowPause'],
                'showFooter': recipe['screens'][i]['showFooter'],
                'showHeader': recipe['screens'][i]['showHeader'],
                'fields': fields,
                'x': str(start_locationX+INCREMENT_X*i),
                'y': str(start_locationY+INCREMENT_Y*2),
            }

            if 'targetReference' in recipe['screens'][i].keys():
                screen_data['targetReference'] = recipe['screens'][i]['targetReference']

            screen = create_tag(element_type='screens', data=screen_data)
            case_details_screens.append(screen)

        FLOW_LAYOUT['screens'] = case_details_screens

    FLOW_LAYOUT['variables'] = [
        create_tag(element_type='variables', data={
            'name': variable['name'],
            'dataType': variable['dataType'],
            'isCollection': variable['isCollection'],
            'isInput': variable['isInput'],
            'isOutput': variable['isOutput'],
        })
        if variable['dataType'] == 'String' else

        create_tag(element_type='variables', data={
            'name': variable['name'],
            'dataType': variable['dataType'],
            'isCollection': variable['isCollection'],
            'isInput': variable['isInput'],
            'isOutput': variable['isOutput'],
            'objectType': variable['objectType']
        })
        if variable['dataType'] == 'SObject' else None

        for variable in recipe['variables']
    ]

    if 'actionCalls' in recipe.keys():
        actCalls = []
        for actionCall in recipe['actionCalls']:
            actCalls.append(create_tag(element_type='actionCalls', data={
                'name': actionCall['name'],
                'label': actionCall['label'],
                'targetReference_c': actionCall['targetReference_c'],
                'inputParameters': actionCall['inputParameters']
            }))
        FLOW_LAYOUT['actionCalls'] = actCalls

    if 'formulas' in recipe.keys():
        for formula in recipe['formulas']:
            data = {
                'name': formula['name'],
                'expression': formula['expression']
            }

            if 'dataType' in formula.keys():
                data['dataType'] = formula['dataType']
            
            FLOW_LAYOUT['formulas'].append(create_tag(element_type='formulas', data=data))

    if 'textTemplates' in recipe.keys():
        for ttemplate in recipe['textTemplates']:
            data = {
                'name': ttemplate['name'],
                'text': ttemplate['text'],
            }
            if 'isViewedAsPlainText' in ttemplate.keys():
                data['isViewedAsPlainText'] = ttemplate['isViewedAsPlainText']

            FLOW_LAYOUT['textTemplates'].append(create_tag(element_type='textTemplates', data=data))

    if 'status' in recipe.keys():
        status = tree.find(f'./{ns}status')
        status = status if status is not None else tree.find(f'./status')

        if status is not None:
            status.text = recipe['status']
        else:
            FLOW_LAYOUT['status'].append(create_tag(element_type='status', data={
                'status': recipe['status']
            }))

    if 'recordUpdates' in recipe.keys():
        for recordUpdate in recipe['recordUpdates']:
            FLOW_LAYOUT['recordUpdates'].append(
                create_tag(element_type='recordUpdates', data={
                    'name': recordUpdate['name'],
                    'label': recordUpdate['label'],
                    'filterLogic': recordUpdate['filterLogic'],
                    'filters': recordUpdate['filters'],
                    'inputAssignments': recordUpdate['inputAssignments'],
                    'object': recordUpdate['object'],
                })
            )

    # Build xml tree
    tree = build_flow(FLOW_LAYOUT, flow)
    root = tree.getroot()
    ns = namespace(root)

    # save(tree, filename="new_flow.xml")
    save(tree, filename="D:/Users/HeanG/git-repos/211-san-diego/force-app/main/default/flows/HelpDesk_Subflow_Email_Notification.flow-meta.xml")