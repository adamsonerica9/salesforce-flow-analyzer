import functools
import os
import xml.etree.ElementTree as ET
from typing import List

from sal_119 import (EMAIL_TARGETS, FLOW_LAYOUT, INCREMENT_X, INCREMENT_Y,
                     create_tag, get_support_team_name, namespace,
                     recordCrete_inputAssignments_sorter, save, xml_printer)
from screen_builder import build_flow
from xml_tag_creator import *

ET.register_namespace("", "http://soap.sforce.com/2006/04/metadata")


if __name__ == "__main__":

    flow_folder = 'D:/Users/HeanG/git-repos/211-san-diego/force-app/main/default/flows'

    for flow_file in os.listdir(flow_folder):
        skip = True
        new_choice = False

        if any([forbidden in flow_file for forbidden in ['MainFlow.flow', 'Subflow_Email_Notification.flow']]):
            continue

        flow_to_update = os.path.join(flow_folder, flow_file)
        tree = ET.parse(flow_to_update)

        root = tree.getroot()
        ns = namespace(root)
        tree.find(f'./{ns}status').text = 'Active'
        tree.find(f'./{ns}description').text = 'Forbid going back once Final screen reached.'

        for screentype in ['upload', 'finalization']:
            screens: List[ET.Element] = root.findall(f'./{ns}screens')
            screens = list(filter(lambda screen: 'upload' in screen.find(f'./{ns}name').text.lower(), screens))

            def assign(tag, value):
                tag.text = value

            [ assign(screen.find(f'./{ns}allowBack'), 'false') for screen in screens]
            # print(f"For {flow_file}:\n{'\n\t'.join([screen.find(f'./{ns}name').text for screen in screens])}")
            print(f"For {flow_file}: We have {len(screens)} branches")
        print()

        save(tree, filename=flow_to_update)

