import copy
import re
import xml.etree.ElementTree as ET
from sal_119 import run, save

__TAGNAME_PATTERN__ = r'(\{.*\})(.*)'
__SCREENS__ = "screens"


def namespace(element):
    m = re.match(__TAGNAME_PATTERN__, element.tag)
    return m.group(1) if m else ''


def tagname(element):
    m = re.match(__TAGNAME_PATTERN__, element.tag)
    return m.group(2) if m else ''


def getChildByTagName(element, tagname=''):
    if not tagname:
        return None


def build_urgency_field(tag_builder_info, index=0):
    tagobject = None
    tagobjects = []

    for tagname, tagcontent in tag_builder_info.items():
        # print(f"{'\t'*index}processing {tagname} tag")
        tagobject = ET.Element(tagname)

        if type(tagcontent) == str:
            tagobject.text = tagcontent
            # print(f"{'\t'*index}tag {tagobject.tag} with '{tagobject.text}' value constructed")
        else:
            # print(f"{'\t'*index}constructing {tagobject.tag} with the following value:")
            subelements = build_urgency_field(copy.deepcopy(tagcontent), index=index+1)
            # print(f"{'\t'*index}assigning {subelement.tag} to {tagobject.tag}")

            for se in subelements:
                tagobject.append(se)

        tagobjects.append(tagobject)

    return tagobjects


if __name__ == "__main__":
    ET.register_namespace("", "http://soap.sforce.com/2006/04/metadata")

    tree = ET.parse("output\HelpDesk_MainFlow.flow-meta.xml")
    root = tree.getroot()
    ns = namespace(root)

    old_names = {
        "Lvl1Cat_GralSrvQuestionFeedback_CreateCase" : "CreateCase_when_Level1CategoryIs_GeneralServiceQuestion",
        "Lvl1Cat_PartnerRequestingCallback_Create_Case" : "CreateCase_when_Level1CategoryIs_PartnerRequestingCallback",
        "Lvl1Cat_RecommendedAgengyOrSrv_Create_Case" : "CreateCase_when_Level1CategoryIs_RecommendedAgencyOrService",
        "Lvl1Cat_SrvEditFeedback_Create_Case" : "CreateCase_when_Level1CategoryIs_ServiceEditOrFeedback",

        # "CreateCase_when_Lvl1CategoryIs_AggregateSummaryClientData",
        # "CreateCase_when_Lvl1CategoryIs_DetailLevelClientData",
        # "CreateCase_when_Level1CategoryIs_ResourceDatabaseServiceListing",
        # "CreateCase_when_Level1CategoryIs_CIEPartnerUtilizationReport"
    }

    names = (
        "CreateCase_when_Level1CategoryIs_GeneralServiceQuestion",
        "CreateCase_when_Level1CategoryIs_PartnerRequestingCallback",
        "CreateCase_when_Level1CategoryIs_RecommendedAgencyOrService",
        "CreateCase_when_Level1CategoryIs_ServiceEditOrFeedback",

        "CreateCase_when_Lvl1CategoryIs_AggregateSummaryClientData",
        "CreateCase_when_Lvl1CategoryIs_DetailLevelClientData",
        "CreateCase_when_Level1CategoryIs_ResourceDatabaseServiceListing",
        "CreateCase_when_Level1CategoryIs_CIEPartnerUtilizationReport",
    )

    to_delete_field_names = (
        "Field_CaseUrgency_when_IRAIs_ResourceDB",
        "Field_CaseUrgency_when_IRAIs_DataRequest",
    )


    # for name in names:
    #     screens = tree.findall(f".//*[{ns}name='{name}']")
    #     when = name.replace("CaseDetails_", "").replace("CaseDetail_", "")

    #     tag_builder_input = {
    #         "fields": {
    #             "name": "Field_CaseUrgency_" + when,
    #             "choiceReferences": "Urgency_ChoiceSet",
    #             "dataType": "String",
    #             "defaultValue": {
    #                 "stringValue": "Medium"
    #             },
    #             "fieldText": "The level of urgency of the request",
    #             "fieldType": "DropdownBox",
    #             "isRequired": "true" if name in ["CaseDetails_when_Level2CategoryIs_GenericITIncident", "CaseDetails_when_Level2CategoryIs_PrintingIncident"] else "false",
    #         }
    #     }

    #     choice_fields = build_urgency_field(tag_builder_info=tag_builder_input)

    #     index = -1
    #     index_of_field = 1
    #     for child in screens[0]:
    #         index += 1
    #         if tagname(child) == "fields":
    #             index_of_field += 1

    #             if index_of_field == 2:
    #                 [screens[0].insert(index+index_of_field, choice_field) for choice_field in choice_fields]
    #                 index_of_field = 1
    #                 break

    whenses = [name.replace("CaseDetails_", "").replace("CaseDetail_", "") for name in names]

    for old, new in old_names.items():
        for recordCreates in tree.findall(f"./{ns}recordCreates"):
            name = recordCreates.find(f"{ns}name")
            if name.text == old :
                name.text = new

        for targetReference in tree.findall(f"./{ns}screens/{ns}connector/{ns}targetReference"):
            text = targetReference.text
            if text == old:
                targetReference.text = new

        for elementReference in tree.findall(f"{ns}screens/{ns}fields/{ns}inputParameters/{ns}value/{ns}elementReference"):
            if elementReference.text == old:
                elementReference.text = new

    for name in names:
        screen = tree.find(f"./{ns}recordCreates/[{ns}name='{name}']")
        to_remove = screen.find(f"./{ns}inputAssignments/[{ns}field='Urgency__c']") if screen else None
        screen.remove(to_remove) if to_remove is not None else None

    for field_name in to_delete_field_names:
        for screen in tree.findall(f"./{ns}screens"):
            for field in screen.findall(f"{ns}fields"):
                name = field.find(f"{ns}name")
                if name.text == field_name:
                    screen.remove(field)


        # does_need_urgency = any([screen.findall(f"./[{ns}name='CreateCase_{when}']") for when in whenses])

        # if does_need_urgency:
        #     inputAssignments = ET.Element("inputAssignments")
        #     field = ET.Element("field")
        #     field.text = "Urgency__c"
        #     value = ET.Element("value")
        #     elementReference = ET.Element("elementReference")
        #     elementReference.text = "Field_CaseUrgency_" + screen.find(f"./{ns}name").text.replace("CreateCase_", "")
        #     value.append(elementReference)
        #     inputAssignments.append(field)
        #     inputAssignments.append(value)
            
        #     index_onwhichto_insert = -1
        #     detected = False
        #     for child in screen:
        #         index_onwhichto_insert += 1
        #         if tagname(child) == "inputAssignments":
        #             detected = True
        #         if detected and tagname(child) != "inputAssignments":
        #             screen.insert(index_onwhichto_insert, inputAssignments)
        #             break

    run(tree, root, ns)

    save(tree)