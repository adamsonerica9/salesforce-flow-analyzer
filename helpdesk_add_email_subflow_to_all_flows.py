import json
import os
from venv import create
from webbrowser import get
import xml.etree.ElementTree as ET
import xml

import yaml

from sal_119 import (FLOW_LAYOUT, INCREMENT_X, INCREMENT_Y, create_tag, get_support_team_name,
                     namespace, save, xml_printer)
from screen_builder import build_flow

ET.register_namespace("", "http://soap.sforce.com/2006/04/metadata")

email_targets = {
    'Salesforce': 'salesforcehelpdesk@211sandiego.org',
    'IT': 'helpdesk@211sandiego.org',
    'Informatics': 'helpdesk@211sandiego.org',
    'Resource': 'resourcecenter@211sandiego.org ',
    'Data': 'datarequest@211sandiego.org ',
    'Facilities': 'facilities@211sandiego.org',
    'CIE Local': 'ciehelpdesk@211sandiego.org',
    'CIE National': 'CIE@ciesandiego.org',
}


if __name__ == "__main__":

    flow_to_update = 'D:/Users/HeanG/git-repos/211-san-diego/force-app/main/default/flows/HelpDesk_Technology_Issues.flow-meta.xml'
    support_team_email = 'helpdesk@211sandiego.org'
    flowName = 'HelpDesk_Subflow_Email_Notification'
    parent_node_prefix = 'FileUpload_when_'
    email_subflow_prefix = 'Subflow_EmailNotif_when_'
    support_team_name = 'IT'

    tree = ET.parse(flow_to_update)
    root = tree.getroot()
    ns = namespace(root)
    tree.find(f'./{ns}status').text = 'Draft'
    tree.find(
        f'./{ns}description').text = 'Add Email Notif Subflow to all paths'

    screens = tree.findall(f'./{ns}screens')
    fileUploads = filter(lambda screen: parent_node_prefix in screen.find(
        f'./{ns}name').text, screens)
    fileUploads_without_email = filter(lambda screen: screen.find(
        f'./{ns}connector/{ns}targetReference') is not None and email_subflow_prefix not in screen.find(f'./{ns}connector/{ns}targetReference').text, fileUploads)
    tree.find(
        f'./{ns}description').text = 'Add Email Notification Subflow to all paths'
    tree.find(f'./{ns}status').text = 'Draft'

    for fu in fileUploads_without_email:
        current_when = fu.find(
            f'./{ns}name').text.replace(parent_node_prefix, '')

        formula_tag_data = {
            'name': f'CaseSubjectFormula_when_' + current_when,
            'dataType': 'String',
            'expression': '{!Field_Subject_when_' + current_when + '}'
        }
        formula = create_tag(element_type='formulas', data=formula_tag_data)
        FLOW_LAYOUT['formulas'].append(formula)

        support_team = get_support_team_name(tree, 'CreateCase_when_' + current_when)

        data = {
            'name': email_subflow_prefix + current_when,
            'label': 'Subflow Email Notification',
            'flowName': flowName,
            'targetReference': fu.find(f'./{ns}connector/{ns}targetReference').text,
            'inputAssignments': [
                {
                    'name': 'In_EmailSubject',
                    'stringValue': f'Support from HelpDesk {support_team_name} for ' + '"{!' + formula_tag_data['name'] + '}"',
                },
                {
                    'name': 'In_Sender_EmailAddress',
                    'stringValue': email_targets[support_team],
                },
                {
                    'name': 'In_CaseId',
                    'elementReference': 'CreateCase_when_' + current_when,
                },
            ]
        }

        fu.find(f'./{ns}connector/{ns}targetReference').text = data['name']

        subflow = create_tag(element_type='subflows', data=data)
        FLOW_LAYOUT['subflows'].append(subflow)

    # Build xml tree
    tree = build_flow(FLOW_LAYOUT, root)
    root = tree.getroot()
    ns = namespace(root)

    save(tree, filename=flow_to_update)
