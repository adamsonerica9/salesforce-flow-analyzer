import copy
import json
import re
import xml.etree.ElementTree as ET
from html import escape as esc
from os import name
from typing import List
from xml.sax.saxutils import escape

__TAGNAME_PATTERN__ = r'(\{.*\})(.*)'
__SCREENS__ = "screens"

FLOW_LAYOUT = {
    "actionCalls": [],
    "apiVersion": [],
    "choices": [],
    "constants": [],
    "decisions": [],
    "description": [],
    "dynamicChoiceSets": [],
    "environments": [],
    "formulas": [],
    "interviewLabel": [],
    "isAdditionalPermissionRequiredToRun": [],
    "label": [],
    "processMetadataValues": [],
    "processType": [],
    "recordCreates": [],
    "recordLookups": [],
    "recordUpdates": [],
    "runInMode": [],
    "screens": [],
    "start": [],
    "status": [],
    "subflows": [],
    "textTemplates": [],
    "variables": [],
}

SIMPLE_ONCE_TAG_NAMES = ("apiVersion", "description", "environments", "interviewLabel",
                         "label", "processType", "runInMode", "start", "status")

EMAIL_TARGETS = {
    'Salesforce': 'salesforcehelpdesk@211sandiego.org',
    'IT': 'helpdesk@211sandiego.org',
    'Resource': 'resourcecenter@211sandiego.org',
    'Resource Center': 'resourcecenter@211sandiego.org',
    'Data': 'datarequest@211sandiego.org',
    'Informatics': 'datarequest@211sandiego.org',
    'Facilities': 'facilities@211sandiego.org',
    'CIE Local': 'ciehelpdesk@211sandiego.org',
    'CIE National': 'CIE@ciesandiego.org',
}

INCREMENT_X = 264
INCREMENT_Y = 216

FLOW_NAMESPACE = "http://soap.sforce.com/2006/04/metadata"


def namespace(element):
    m = re.match(__TAGNAME_PATTERN__, element.tag)
    return m.group(1) if m else ''


def tagname(element):
    m = re.match(__TAGNAME_PATTERN__, element.tag)
    return m.group(2) if m else ''


def getChildByTagName(element, tagname=''):
    if not tagname:
        return None


def build_urgency_field(tag_builder_info, index=0):
    tagobject = None
    tagobjects = []

    for tagname, tagcontent in tag_builder_info.items():
        tagobject = ET.Element(tagname)

        if type(tagcontent) == str:
            tagobject.text = tagcontent
        else:
            subelements = build_urgency_field(copy.deepcopy(tagcontent), index=index + 1)

            for se in subelements:
                tagobject.append(se)

        tagobjects.append(tagobject)

    return tagobjects


def tag_sorter(tag1: ET.Element, tag2: ET.Element):
    ns = namespace(tag1)

    def dict_sorter(pair):
        key, val = pair
        return val is not False

    eval_map = {
        1: tag1.find(f'{ns}name').text > tag2.find(f'./{ns}name').text,
        0: tag1.find(f'{ns}name').text == tag2.find(f'./{ns}name').text,
        -1: tag1.find(f'{ns}name').text < tag2.find(f'./{ns}name').text,
    }
    result = dict(filter(dict_sorter, eval_map.items()))

    return list(result.keys())[0]


def recordCrete_inputAssignments_sorter(tag: ET.Element):
    ns = namespace(tag)
    result = None

    if tag.find(f'{ns}field') is not None:
        result = tag.find(f'{ns}field').text
    else:
        result = tag.find(f'field').text

    return result

def create_tag(element_type="choices", data={}) -> ET.Element:
    # if any([not param for param in [element_type, data]]):
    #     return None

    tag = None

    if element_type == "choices":
        """
            <choices>
                <name>Level1Cat_GralSrvQuestionOrFeedback_Choice</name>
                <choiceText>General Service Question or Feedback</choiceText>
                <dataType>String</dataType>
                <value>
                    <stringValue>General Service Question or Feedback</stringValue>
                </value>
            </choices>
        """
        tag = ET.Element(element_type)
        name = ET.Element("name")
        text = ET.Element("choiceText")
        dtype = ET.Element("dataType")
        value = ET.Element("value")
        typeValueTag = ET.Element(data["typeValueTag"])

        typeValueTag.text = data["typeValueText"]
        dtype.text = data["dataType"]
        text.text = data["choiceText"]
        name.text = data["name"]

        value.append(typeValueTag)
        [tag.append(subTag) for subTag in [name, text, dtype, value]]

    elif element_type == "decision_rules":
        tag = ET.Element("rules")
        name = ET.Element("name")
        conditionLogic = ET.Element("conditionLogic")
        conditions = ET.Element("conditions")
        leftValueReference = ET.Element("leftValueReference") if 'leftValueReference' in data.keys() and data['leftValueReference'] is not None else None
        operator = ET.Element("operator") if 'operator' in data.keys() else None
        rightValue = ET.Element("rightValue") if 'rightValue' in data.keys() else None
        elementReference = ET.Element("elementReference") if 'rightValueElementReference' in data.keys() else None
        stringValue = ET.Element("stringValue") if 'stringValue' in data.keys() else None
        label = ET.Element("label")
        targetReference = ET.Element("targetReference") if 'targetReference_c' in data.keys() and data['targetReference_c'] is not None else None
        connector = ET.Element("connector") if targetReference is not None else None

        name.text = data["name"]
        conditionLogic.text = data["conditionLogic"]
        label.text = data["label"]

        if targetReference is not None:
            targetReference.text = data["targetReference_c"]
            connector.append(targetReference)

        if leftValueReference is not None:
            leftValueReference.text = data["leftValueReference"]
            operator.text = data["operator"]

            if elementReference is not None:
                elementReference.text = data["rightValueElementReference"]
            if stringValue is not None:
                stringValue.text = data['stringValue']

            rightValue.append(elementReference) if elementReference is not None else None
            rightValue.append(stringValue) if stringValue is not None else None
            [conditions.append(subTag) for subTag in [leftValueReference, operator, rightValue]]

            [tag.append(subTag) for subTag in [name, conditionLogic,conditions, connector, label] if subTag is not None]

        elif 'conditions' in data.keys():
            conds = []

            for condition in data['conditions']:
                conditions = ET.Element('conditions')

                leftValueReference = ET.Element("leftValueReference") if 'leftValueReference' in condition.keys() else None
                operator = ET.Element("operator") if 'operator' in condition.keys() else None
                rightValue = ET.Element('rightValue')
                rightValueType = ET.Element(condition['rightValueType'])

                leftValueReference.text = condition['leftValueReference']
                operator.text = condition['operator']
                rightValueType.text = condition['rightValue']
                rightValue.append(rightValueType)

                _ = {conditions.append(element) for element in [leftValueReference, operator, rightValue] if element is not None}

                conds.append(conditions)

            [tag.append(subTag) for subTag in [name, conditionLogic]]
            [tag.append(subTag) for subTag in conds]
            [tag.append(subTag) for subTag in [connector, label] if subTag is not None]

    elif element_type == "choiceReferences":
        tag = ET.Element("choiceReferences")
        tag.text = data["text"]

    elif element_type == "faultConnector":
        tag = ET.Element(element_type)
        targetReference = ET.Element("targetReference")
        isGoTo_fc = ET.Element("isGoTo") if data["isGoTo"] else None

        targetReference.text = data["targetReference"]
        if isGoTo_fc is not None:
            isGoTo_fc.text = "true"

        tag.append(isGoTo_fc) if isGoTo_fc is not None else None
        tag.append(targetReference)

    elif element_type == "input_variable":
        """
        <variables>
            <name>In_Level1Cat_when_IRAIs_Technology</name>
            <dataType>String</dataType>
            <isCollection>false</isCollection>
            <isInput>true</isInput>
            <isOutput>false</isOutput>
        </variables>
        """
        tag = ET.Element("variables")
        name = ET.Element("name")
        dataType = ET.Element("dataType")
        isCollection = ET.Element("isCollection")
        isInput = ET.Element("isInput")
        isOutput = ET.Element("isOutput")
        objectType = ET.Element("objectType") if "objectType" in data.keys() and data["objectType"] is not None else None

        name.text = data["name"]
        dataType.text = data["dataType"]
        isCollection.text = data["isCollection"]
        isInput.text = data["isInput"]
        isOutput.text = data["isOutput"]
        if objectType is not None:
            objectType.text = data["objectType"]

        _ = {tag.append(element) for element in [name, dataType, isCollection, isInput, isOutput, objectType] if element is not None}

    elif element_type == "screen_field":
        if data is None:
            raise AttributeError("'data' is empty")

        tag = ET.Element("fields")
        name = ET.Element("name")
        fieldType = ET.Element("fieldType")
        isRequired = ET.Element("isRequired") if 'isRequired' in data.keys() else None

        name.text = data["name"]
        fieldType.text = data["fieldType"]
        if isRequired is not None:
            isRequired.text = data["isRequired"] if type(data["isRequired"]) == str else str(data["isRequired"]).lower()

        if "extensionName" in data.keys() and data["extensionName"] == "flowruntime:lookup":
            extensionName = ET.Element("extensionName")
            inputsOnNextNavToAssocScrn = ET.Element("inputsOnNextNavToAssocScrn")
            storeOutputAutomatically = ET.Element("storeOutputAutomatically")

            extensionName.text = data["extensionName"]
            inputsOnNextNavToAssocScrn.text = data["inputsOnNextNavToAssocScrn"]
            storeOutputAutomatically.text = data["storeOutputAutomatically"] if type(data["storeOutputAutomatically"]) == str else str(data["storeOutputAutomatically"]).lower()

            _ = {tag.append(child) for child in [name, extensionName, fieldType]}

            inputParms = []

            for inputParameter in data["inputParameters"]:
                inputParamters = ET.Element("inputParameters")
                name = ET.Element("name")
                value = ET.Element("value")
                typeValue = ET.Element(inputParameter["typeValueTag"])

                name.text = inputParameter["name"]
                typeValue.text = inputParameter["typeValueText"] if type(
                    inputParameter["typeValueText"]) == str else str(inputParameter["typeValueText"]).lower()

                value.append(typeValue)
                _ = {inputParamters.append(child) for child in [name, value]}

                inputParms.append(inputParamters)

            _ = {tag.append(inputParm) for inputParm in inputParms}
            _ = {tag.append(child) for child in [inputsOnNextNavToAssocScrn, isRequired, storeOutputAutomatically] if child is not None}

        elif 'extensionName' in data.keys() and data['extensionName'] == 'flowruntime:email':
            extensionName = ET.Element("extensionName")
            inputsOnNextNavToAssocScrn = ET.Element("inputsOnNextNavToAssocScrn")
            storeOutputAutomatically = ET.Element("storeOutputAutomatically")

            extensionName.text = data["extensionName"]
            inputsOnNextNavToAssocScrn.text = data["inputsOnNextNavToAssocScrn"]
            storeOutputAutomatically.text = data["storeOutputAutomatically"] if type(data["storeOutputAutomatically"]) == str else str(data["storeOutputAutomatically"]).lower()

            _ = {tag.append(child) for child in [name, extensionName, fieldType]}

            inputParms = []

            for inputParameter in data["inputParameters"]:
                inputParamters = ET.Element("inputParameters")
                name = ET.Element("name")
                value = ET.Element("value")
                typeValue = ET.Element(inputParameter["typeValueTag"])

                name.text = inputParameter["name"]
                typeValue.text = inputParameter["typeValueText"] if type(inputParameter["typeValueText"]) == str else str(inputParameter["typeValueText"]).lower()

                value.append(typeValue)
                _ = {inputParamters.append(child) for child in [name, value]}

                inputParms.append(inputParamters)

            _ = {tag.append(inputParm) for inputParm in inputParms}
            _ = {tag.append(child) for child in [
                inputsOnNextNavToAssocScrn, isRequired, storeOutputAutomatically] if child is not None}

        elif "extensionName" in data.keys() and data["extensionName"] == "forceContent:fileUpload":
            extensionName = ET.Element("extensionName")
            inputsOnNextNavToAssocScrn = ET.Element("inputsOnNextNavToAssocScrn")
            storeOutputAutomatically = ET.Element("storeOutputAutomatically")

            extensionName.text = data["extensionName"]
            inputsOnNextNavToAssocScrn.text = data["inputsOnNextNavToAssocScrn"] if type(data["inputsOnNextNavToAssocScrn"]) == str else str(data["inputsOnNextNavToAssocScrn"]).lower()
            storeOutputAutomatically.text = data["storeOutputAutomatically"] if type(data["storeOutputAutomatically"]) == str else str(data["storeOutputAutomatically"]).lower()

            _ = {tag.append(child) for child in [name, extensionName, fieldType]}

            inputParms = []

            for inputParameter in data["inputParameters"]:
                inputParamters = ET.Element("inputParameters")
                name = ET.Element("name")
                value = ET.Element("value")
                typeValue = ET.Element(inputParameter["typeValueTag"]) if "typeValueTag" in inputParameter.keys() else None
                elementReference = ET.Element("elementReference") if "elementReference" in inputParameter.keys() else None

                name.text = inputParameter["name"]
                if typeValue is not None:
                    typeValue.text = inputParameter["typeValueText"] if type(inputParameter["typeValueText"]) == str else str(inputParameter["typeValueText"]).lower()
                if elementReference is not None:
                    elementReference.text = inputParameter["elementReference"]

                value.append(typeValue) if typeValue is not None else (value.append(elementReference) if elementReference is not None else None)
                _ = {inputParamters.append(child) for child in [name, value]}

                inputParms.append(inputParamters)

            _ = {tag.append(inputParm) for inputParm in inputParms}
            _ = {tag.append(child) for child in [
                inputsOnNextNavToAssocScrn, isRequired, storeOutputAutomatically] if child is not None}

        elif data["fieldType"] == "LargeTextArea":
            fieldText = ET.Element("fieldText")

            fieldText.text = data["fieldText"]

            _ = {tag.append(child) for child in [name, fieldText, fieldType, isRequired] if child is not None}

        elif data["fieldType"] == "InputField":
            dataType = ET.Element("dataType")

            dataType.text = data["dataType"]

            if dataType.text in ["DateTime", "String", "Date"]:
                fieldText = ET.Element("fieldText")
                fieldText.text = data["fieldText"]

                _ = {tag.append(child) for child in [name, dataType, fieldText, fieldType, isRequired] if child is not None}

        elif data["fieldType"] in ["MultiSelectPicklist", 'DropdownBox']:
            tag = ET.Element("fields")
            name = ET.Element('name')
            choiceReferences = ET.Element("choiceReferences")
            dataType = ET.Element("dataType")
            fieldText = ET.Element("fieldText")
            fieldType = ET.Element("fieldType")
            isRequired = ET.Element("isRequired")

            name.text = data['name']
            choiceReferences.text = data["choiceReferences"]
            dataType.text = data["dataType"]
            fieldText.text = data["fieldText"]
            fieldType.text = data["fieldType"]
            isRequired.text = data["isRequired"] if type(data["isRequired"]) == str else str(data["isRequired"])

            _ = {tag.append(child) for child in [name, choiceReferences, dataType, fieldText, fieldType, isRequired] if child is not None}

        elif data['fieldType'] == 'DisplayText':
            tag = ET.Element('fields')
            name = ET.Element('name')
            fieldType = ET.Element('fieldType')
            fieldText = ET.Element('fieldText')

            name.text = data['name']
            fieldType.text = data['fieldType']
            fieldText.text = data['fieldText']

            _ = [tag.append(child) for child in [name, fieldType, fieldText] if child is not None]

        else:
            print('Warning: screen_field returned is None.')
            tag = None

    elif element_type == "dynamicChoiceSets":
        tag = ET.Element('dynamicChoiceSets')
        name = ET.Element('name')
        dataType = ET.Element('dataType')
        displayField = ET.Element('displayField')
        objekt = ET.Element('object')
        picklistField = ET.Element('picklistField')
        picklistObject = ET.Element('picklistObject')

        name.text = data['name']
        dataType.text = data['dataType']
        displayField.set('xsi:nil', data['displayField-xsi:nil'] if type(data['displayField-xsi:nil']) == str else str(data['displayField-xsi:nil']).lower())
        objekt.set('xsi:nil', data['object-xsi:nil'] if type(data['object-xsi:nil']) == str else str(data['object-xsi:nil']).lower())
        picklistField.text = data['picklistField']
        picklistObject.text = data['picklistObject']

        _ = {tag.append(child) for child in [name, dataType, displayField, objekt, picklistField, picklistObject]}

    elif element_type == "screens":
        tag = ET.Element("screens")
        name = ET.Element("name")
        label = ET.Element("label")
        locationX = ET.Element("locationX")
        locationY = ET.Element("locationY")
        allowBack = ET.Element("allowBack")
        allowFinish = ET.Element("allowFinish")
        allowPause = ET.Element("allowPause")
        connector = ET.Element("connector") if "targetReference" in data.keys() else None
        targetReference = ET.Element("targetReference") if "targetReference" in data.keys() else None
        showFooter = ET.Element("showFooter")
        showHeader = ET.Element("showHeader")

        name.text = data["name"]
        label.text = data["label"]
        allowBack.text = data["allowBack"] if type(data["allowBack"]) == str else str(data["allowBack"]).lower()
        allowFinish.text = data["allowFinish"] if type(data["allowFinish"]) == str else str(data["allowFinish"]).lower()
        allowPause.text = data["allowPause"] if type(data["allowPause"]) == str else str(data["allowPause"]).lower()
        # connector.text = data["connector"]
        if targetReference is not None:
            targetReference.text = data["targetReference"]
        showFooter.text = data["showFooter"] if type(data["showFooter"]) == str else str(data["showFooter"]).lower()
        showHeader.text = data["showHeader"] if type(data["showHeader"]) == str else str(data["showHeader"]).lower()
        locationX.text = data["x"]
        locationY.text = data["y"]

        connector.append(targetReference) if targetReference is not None else None
        fields = data["fields"]

        _ = {tag.append(child) for child in [name, label, locationX, locationY, allowBack, allowFinish, allowPause, connector] if child is not None}
        _ = {tag.append(field) for field in fields if field is not None}
        _ = {tag.append(child) for child in [showFooter, showHeader]}

    elif element_type == "recordCreates":
        tag = ET.Element(element_type)
        name = ET.Element("name")
        label = ET.Element("label")
        locationX = ET.Element("locationX")
        locationY = ET.Element("locationY")
        connector = ET.Element("connector") if 'targetReference_c' in data.keys() else None
        targetReference_c = ET.Element("targetReference") if 'targetReference_c' in data.keys() else None
        faultConnector = ET.Element("faultConnector") if 'targetReference_fc' in data.keys() else None
        isGoTo_fc = ET.Element("isGoTo") if "isGoTo_fc" in data.keys() and data["isGoTo_fc"] == "true" else None
        targetReference_fc = ET.Element("targetReference") if 'targetReference_fc' in data.keys() else None
        objekt = ET.Element("object")
        storeOutputAutomatically = ET.Element("storeOutputAutomatically")

        name.text = data["name"]
        label.text = data["label"]
        if targetReference_c is not None:
            targetReference_c.text = data["targetReference_c"]
        if targetReference_fc is not None:
            targetReference_fc.text = data["targetReference_fc"]
        objekt.text = data["object"]
        storeOutputAutomatically.text = data["storeOutputAutomatically"] if type(data["storeOutputAutomatically"]) == str else str(data["storeOutputAutomatically"]).lower()
        locationX.text = data["x"] if type(data["x"]) == str else str(data["x"]).lower()
        locationY.text = data["y"] if type(data["y"]) == str else str(data["y"]).lower()
        if isGoTo_fc is not None:
            isGoTo_fc.text = data["isGoTo_fc"]
        if connector is not None:
            connector.append(targetReference_c)
        if faultConnector is not None:
            faultConnector.append(isGoTo_fc) if isGoTo_fc is not None else None
            faultConnector.append(targetReference_fc) if targetReference_fc is not None else None

        inputs = []
        for inputAssignment in data["inputAssignments"]:
            inputAssignments = ET.Element("inputAssignments")
            field = ET.Element("field")
            value = ET.Element("value")
            valueChild = ET.Element("elementReference") if "elementReference" in inputAssignment.keys() else ET.Element("stringValue") if 'stringValue' in inputAssignment.keys() else None

            field.text = inputAssignment["field"]
            if valueChild is not None:
                valueChild.text = inputAssignment["elementReference"] if "elementReference" in inputAssignment.keys() else (inputAssignment["stringValue"] if "stringValue" in inputAssignment.keys() else None)

            value.append(valueChild) if valueChild is not None else None
            _ = {inputAssignments.append(child) for child in [field, value]}

            inputs.append(inputAssignments)

        _ = {tag.append(child) for child in [name, label, locationX, locationY, connector, faultConnector] if child is not None}
        _ = {tag.append(child) for child in inputs if child is not None}
        _ = {tag.append(child) for child in [objekt, storeOutputAutomatically]}

    elif element_type == "fault_screen":
        """
            <screens>
                <name>Fault_Screen</name>
                <label>Error Message</label>
                <locationX>314</locationX>
                <locationY>566</locationY>
                <allowBack>true</allowBack>
                <allowFinish>true</allowFinish>
                <allowPause>true</allowPause>
                <fields>
                    <name>Fault_Message</name>
                    <fieldText>&lt;p&gt;You have reached an error. Please reach out to your Salesforce administrator with the following error message:&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;{!$Flow.FaultMessage}&lt;/p&gt;</fieldText>
                    <fieldType>DisplayText</fieldType>
                </fields>
                <showFooter>true</showFooter>
                <showHeader>true</showHeader>
            </screens>
        """
        tag = ET.Element("screens")
        name = ET.Element("name")
        label = ET.Element("label")
        locationX = ET.Element("locationX")
        locationY = ET.Element("locationY")
        allowBack = ET.Element("allowBack")
        allowFinish = ET.Element("allowFinish")
        allowPause = ET.Element("allowPause")
        fields = ET.Element("fields")
        fieldname = ET.Element("name")
        fieldText = ET.Element("fieldText")
        fieldType = ET.Element("fieldType")
        showFooter = ET.Element("showFooter")
        showHeader = ET.Element("showHeader")

        name.text = "Fault_Screen"
        label.text = "Error Message"
        allowBack.text = "true"
        allowFinish.text = "true"
        allowPause.text = "true"
        fieldname.text = "Fault_Message"
        fieldText.text = "You have reached an error. Please reach out to your Salesforce administrator with the following error message:\n\n{!$Flow.FaultMessage}"
        fieldType.text = "DisplayText"
        showFooter.text = "true"
        showHeader.text = "true"
        locationX.text = data["x"]
        locationY.text = data["y"]

        _ = {fields.append(child) for child in [fieldname, fieldText, fieldType]}
        _ = {tag.append(child) for child in [name, label, locationX, locationY, allowBack, allowFinish, allowPause, fields, showFooter, showHeader]}

    elif element_type == "decisions":
        tag = ET.Element("decisions")

        if tag is not None:
            name = ET.Element("name")
            label = ET.Element("label")
            locationX = ET.Element("locationX")
            locationY = ET.Element("locationY")
            defaultConnectorLabel = ET.Element("defaultConnectorLabel")
            rules = data["rules"]

            name.text = data["name"]
            label.text = data["label"]
            locationX.text = data["locationX"]
            locationY.text = data["locationY"]
            defaultConnectorLabel.text = data["defaultConnectorLabel"]

            _ = {tag.append(child) for child in [name, label, locationX, locationY, defaultConnectorLabel]}
            _ = {tag.append(child) for child in rules}

    elif element_type == "base_skeleton":
        ET.register_namespace("", "http://soap.sforce.com/2006/04/metadata")

        flow = ET.Element("Flow")
        flow.set("xmlns", "http://soap.sforce.com/2006/04/metadata")
        flow.set("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")

        apiVersion = ET.Element("apiVersion")
        apiVersion.text = "59.0"

        description = ET.Element('description')
        description.text = data['description'] if 'description' in data.keys() else ''

        environments = ET.Element("environments")
        environments.text = "Default"

        interviewLabel = ET.Element("interviewLabel")
        interviewLabel.text = data["label"] + " {!$Flow.CurrentDateTime}"

        label = ET.Element("label")
        label.text = data["label"]

        processMetadataValuesList = []
        processMetadataValues = ET.Element("processMetadataValues")
        processMetadataValues_name = ET.Element("name")
        processMetadataValues_name.text = "BuilderType"
        processMetadataValues_value = ET.Element("value")
        processMetadataValues_value_stringValue = ET.Element("stringValue")
        processMetadataValues_value_stringValue.text = "LightningFlowBuilder"
        processMetadataValues_value.append(processMetadataValues_value_stringValue)
        processMetadataValues.append(processMetadataValues_name)
        processMetadataValues.append(processMetadataValues_value)
        processMetadataValuesList.append(processMetadataValues)

        processMetadataValues = ET.Element("processMetadataValues")
        processMetadataValues_name = ET.Element("name")
        processMetadataValues_name.text = "CanvasMode"
        processMetadataValues_value = ET.Element("value")
        processMetadataValues_value_stringValue = ET.Element("stringValue")
        processMetadataValues_value_stringValue.text = "AUTO_LAYOUT_CANVAS"
        processMetadataValues_value.append(processMetadataValues_value_stringValue)
        processMetadataValues.append(processMetadataValues_name)
        processMetadataValues.append(processMetadataValues_value)
        processMetadataValuesList.append(processMetadataValues)

        processMetadataValues = ET.Element("processMetadataValues")
        processMetadataValues_name = ET.Element("name")
        processMetadataValues_name.text = "OriginBuilderType"
        processMetadataValues_value = ET.Element("value")
        processMetadataValues_value_stringValue = ET.Element("stringValue")
        processMetadataValues_value_stringValue.text = "LightningFlowBuilder"
        processMetadataValues_value.append(processMetadataValues_value_stringValue)
        processMetadataValues.append(processMetadataValues_name)
        processMetadataValues.append(processMetadataValues_value)
        processMetadataValuesList.append(processMetadataValues)

        processType = ET.Element("processType")
        processType.text = "Flow"

        start = ET.Element("start")
        locationX = ET.Element("locationX")
        locationX.text = data['start.locationX']
        locationY = ET.Element("locationY")
        locationY.text = data['start.locationY']
        connector = ET.Element("connector")
        targetReference = ET.Element("targetReference")
        targetReference.text = data['start.targetReference']
        connector.append(targetReference)
        start.append(locationX)
        start.append(locationY)
        start.append(connector)

        status = ET.Element("status")
        status.text = "Draft"

        {flow.append(child) for child in [apiVersion, description, environments, interviewLabel, label]}
        {flow.append(child) for child in processMetadataValuesList}
        {flow.append(child) for child in [processType, start, status]}

        tag = flow

    elif element_type == "variables":
        """
            <variables>
                <name>In_InitialRequestArea</name>
                <dataType>String</dataType>
                <isCollection>false</isCollection>
                <isInput>true</isInput>
                <isOutput>false</isOutput>
            </variables>
        """
        tag = ET.Element('variables')
        name = ET.Element('name')
        dataType = ET.Element('dataType')
        isCollection = ET.Element('isCollection')
        isInput = ET.Element('isInput')
        isOutput = ET.Element('isOutput')
        objectType = ET.Element(
            'objectType') if 'objectType' in data.keys() else None

        name.text = data['name']
        dataType.text = data['dataType']
        isCollection.text = data['isCollection'] if type(data['isCollection']) == str else str(data['isCollection']).lower()
        isInput.text = data['isInput'] if type(data['isInput']) == str else str(data['isInput']).lower()
        isOutput.text = data['isOutput'] if type(data['isOutput']) == str else str(data['isOutput']).lower()
        if objectType is not None:
            objectType.text = data['objectType']

        _ = {tag.append(child) for child in [name, dataType, isCollection, isInput, isOutput, objectType] if child is not None}

    elif element_type == "constants":
        tag = ET.Element('constants')
        name = ET.Element('name')
        dataType = ET.Element('dataType')
        value = ET.Element('value')
        stringValue = ET.Element('stringValue') if 'stringValue' in data.keys() else None
        elementReference = ET.Element('elementReference') if 'elementReference' in data.keys() else None

        if all([element is None for element in [stringValue, elementReference]]):
            raise ValueError("'constant.value' tag can't be empty. It must contain one of them: 'stringValue' or 'elementReference'.")

        name.text = data['name']
        dataType.text = data['dataType']
        if stringValue is not None:
            stringValue.text = data['stringValue']
        if elementReference is not None:
            elementReference.text = data['elementReference']

        value.append(stringValue if stringValue is not None else elementReference if elementReference is not None else None)
        _ = {tag.append(element) for element in [
            name, dataType, value] if element is not None}

    elif element_type == "formulas":
        tag = ET.Element('formulas')
        name = ET.Element('name')
        dataType = ET.Element('dataType')
        expression = ET.Element('expression')

        name.text = data['name']
        dataType.text = data['dataType']
        expression.text = data['expression']

        _ = {tag.append(element) for element in [name, dataType, expression] if element is not None}

    elif element_type == 'actionCalls':
        if 'inputParameters' not in data.keys():
            raise KeyError("'actionCalls.inputParameters' must be specified.")
        elif data['inputParameters'] is None:
            raise KeyError("'actionCalls.inputParameters' should contain at least one subelement. Actually it received empty list.")

        tag = ET.Element('actionCalls')

        name = ET.Element('name')
        label = ET.Element('label')
        locationX = ET.Element('locationX')
        locationY = ET.Element('locationY')
        actionName = ET.Element('actionName')
        actionType = ET.Element('actionType')
        targetReference_c = ET.Element('targetReference') if 'targetReference_c' in data.keys() and data['targetReference_c'] is not None else None
        targetReference_fc = ET.Element('targetReference') if 'targetReference_fc' in data.keys() and data['targetReference_fc'] is not None else None
        connector = ET.Element('connector') if targetReference_c is not None else None
        faultConnector = ET.Element('faultConnector') if targetReference_fc is not None else None
        flowTransactionModel = ET.Element('flowTransactionModel')
        nameSegment = ET.Element('nameSegment')
        versionSegment = ET.Element('versionSegment')

        name.text = data['name']
        label.text = data['label']
        locationX.text = '0'
        locationY.text = '0'
        actionName.text = 'emailSimple'
        actionType.text = 'emailSimple'
        flowTransactionModel.text = 'CurrentTransaction'
        nameSegment.text = 'emailSimple'
        versionSegment.text = '1'

        if targetReference_c is not None:
            targetReference_c.text = data['targetReference_c']
            connector.append(targetReference_c)
        if targetReference_fc is not None:
            targetReference_fc.text = data['targetReference_fc']
            faultConnector.append(targetReference_fc)

        inputParams = []
        for ip in data['inputParameters']:
            inputParameter = ET.Element('inputParameters')
            _name = ET.Element('name')
            value = ET.Element('value')
            stringValue = ET.Element('stringValue') if 'stringValue' in ip.keys() else None
            booleanValue = ET.Element('booleanValue') if 'booleanValue' in ip.keys() else None
            elementReference = ET.Element('elementReference') if 'elementReference' in ip.keys() else None

            if all([element is None for element in [stringValue, booleanValue, elementReference]]):
                raise ValueError("'actionCalls.inputParameters.value' tag can't be empty. It must contain one of them: 'stringValue', 'booleanValue' or 'elementReference'.")

            _name.text = ip['name']
            if elementReference is not None:
                elementReference.text = ip['elementReference']
            if stringValue is not None:
                stringValue.text = ip['stringValue']
            if booleanValue is not None:
                booleanValue.text = ip['booleanValue'] if type(
                    ip['booleanValue']) == str else str(ip['booleanValue']).lower()

            value.append(
                stringValue if stringValue is not None else booleanValue if booleanValue is not None else elementReference if elementReference is not None else None)
            _ = {inputParameter.append(element) for element in [
                _name, value] if element is not None}
            inputParams.append(inputParameter)

        _ = {tag.append(element) for element in [name,label,locationX,locationY,actionName,actionType,connector,faultConnector,flowTransactionModel] if element is not None}
        _ = {tag.append(ip) for ip in inputParams}
        _ = {tag.append(element) for element in [nameSegment, versionSegment]}

    elif element_type == 'textTemplates':
        tag = ET.Element('textTemplates')
        name = ET.Element('name')
        text = ET.Element('text')
        isViewedAsPlainText = ET.Element('isViewedAsPlainText') if 'isViewedAsPlainText' in data.keys() else None

        name.text = data['name']
        text.text = data['text']
        if isViewedAsPlainText is not None:
            isViewedAsPlainText.text = data['isViewedAsPlainText'] if type(data['isViewedAsPlainText']) == str else str(data['isViewedAsPlainText']).lower()

        tag.append(name)
        tag.append(isViewedAsPlainText) if isViewedAsPlainText is not None else None
        tag.append(text)

    elif element_type == 'status':
        tag = ET.Element('status')
        tag.text = data['status']

    elif element_type == 'subflows':
        tag = ET.Element('subflows')
        name = ET.Element('name')
        label = ET.Element('label')
        locationX = ET.Element('locationX')
        locationY = ET.Element('locationY')
        targetReference = ET.Element('targetReference') if 'targetReference' in data.keys() else None
        connector = ET.Element('connector') if targetReference is not None else None
        flowName = ET.Element('flowName')

        name.text = data['name']
        label.text = data['label']
        locationX.text = '1'
        locationY.text = '1'
        if targetReference is not None:
            targetReference.text = data['targetReference']
            connector.append(targetReference)
        flowName.text = data['flowName']

        inputAss = []
        for ip in data['inputAssignments']:
            inputAssignments = ET.Element('inputAssignments')

            _name = ET.Element('name')
            value = ET.Element('value')
            stringValue = ET.Element('stringValue') if 'stringValue' in ip.keys() else None
            elementReference = ET.Element('elementReference') if 'elementReference' in ip.keys() else None

            _name.text = ip['name']

            if stringValue is not None:
                stringValue.text = ip['stringValue']
            if elementReference is not None:
                elementReference.text = ip['elementReference']

            {value.append(element) for element in [stringValue, elementReference] if element is not None}
            {inputAssignments.append(element) for element in [_name, value] if element is not None}
            inputAss.append(inputAssignments)

        {tag.append(element) for element in [name, label, locationX, locationY, connector, flowName] if element is not None}
        {tag.append(element) for element in inputAss if element is not None}

    elif element_type == 'recordUpdates':
        tag = ET.Element('recordUpdates')
        
        label = ET.Element('label')
        name = ET.Element('name')
        locationX = ET.Element('locationX')
        locationY = ET.Element('locationY')
        filterLogic = ET.Element('filterLogic')
        filters: List[ET.Element] = []
        inputAssignments: List[ET.Element] = []
        objekt = ET.Element('object')

        name.text = data['name']
        label.text = data['label']
        locationX.text = '1'
        locationY.text = '1'
        filterLogic.text = data['filterLogic']
        objekt.text = data['object']

        for f in data['filters']:
            filter = ET.Element('filters')

            field = ET.Element('field')
            operator = ET.Element('operator')
            value = ET.Element('value')
            elementReference = ET.Element('elementReference')

            field.text = f['field']
            operator.text = f['operator']
            elementReference.text = f['value.elementReference']

            value.append(elementReference)
            [filter.append(element) for element in [field, operator, value]]
            filters.append(filter)

        for ia in data['inputAssignments']:
            inputAssignment = ET.Element('inputAssignments')

            field = ET.Element('field')
            value = ET.Element('value')
            elementReference = ET.Element('elementReference')

            field.text = ia['field']
            elementReference.text = ia['value.elementReference']

            value.append(elementReference)
            [inputAssignment.append(element) for element in [field, value]]
            inputAssignments.append(inputAssignment)

        
        [tag.append(element) for element in [label, name, locationX, locationY, filterLogic] + filters + inputAssignments + [objekt]]

    else:
        tag = None

    return tag


def deduplicate(tree: ET.ElementTree):
    root = tree.getroot()
    ns = namespace(root)

    for once_tag in SIMPLE_ONCE_TAG_NAMES:
        tags_with_ns = tree.findall(f'{ns}{once_tag}')
        tags_without_ns = tree.findall(
            once_tag) if len(tags_with_ns) == 0 else []
        duplicated_tags = tags_with_ns + tags_without_ns

        if len(duplicated_tags) > 1:
            for i in range(len(duplicated_tags) - 1):
                root.remove(duplicated_tags[i])

    # For processMetadataValues tags
    tags_with_ns = tree.findall(f'{ns}processMetadataValues')
    tags_without_ns = tree.findall('processMetadataValues') if len(tags_with_ns) == 0 else []
    processMDValuesTags = tags_with_ns + tags_without_ns
    processMDValTags_by_name = {}

    for element in processMDValuesTags:
        name = element.find(f'./{ns}name')
        name = element.find(f'./name') if name is None else name

        if name is not None:
            if name.text not in processMDValTags_by_name.keys():
                processMDValTags_by_name[name.text] = []
            processMDValTags_by_name[name.text].append(element)

    if processMDValTags_by_name is not None:
        for tag_name, processTags in processMDValTags_by_name.items():
            for i in range(len(processTags) - 1):
                root.remove(processTags[i])
                print(f'Removing {tag_name} processMetadataValues tag')


def xml_printer(tag):
    xml = ET.ElementTree(tag)
    ET.indent(xml, '    ')
    print(ET.dump(xml))


def save(tree, filename="output/modified.xml"):
    if not tree:
        raise ValueError("'tree' is empty")

    ET.indent(tree, '    ')
    tree.write(
        filename,
        xml_declaration=True,
        encoding='UTF-8',
        method='xml'
    )


def escape_html(tree):
    root = tree.getroot()
    ns = namespace(root)
    tagnames = ("label", "description")
    for tagname in tagnames:
        for element in tree.findall(f"{ns}{tagname}"):
            element.text = escape(data=element.text, entities={'"': "&quot;"})  # esc(element.text)#


def get_support_team_name(tree: ET.ElementTree, recordCreates_apiName: str):
    ns = namespace(tree.getroot())
    support_team__c = 'Support_Team__c'

    recordCreates: List[ET.Element] = tree.findall(f'./{ns}recordCreates')
    recordCreates = list(filter(lambda element: element.find(f'./{ns}name').text == recordCreates_apiName, recordCreates))

    inputAss: List[ET.Element] = recordCreates[0].findall(f'./{ns}inputAssignments')
    inputAss = list(filter(lambda element: element.find(f'./{ns}field').text == support_team__c, inputAss))

    stringValue = inputAss[0].find(f'./{ns}value/{ns}stringValue').text

    return stringValue


def run(tree, root, ns):

    choiceTag = create_tag(data={
        "typeValueTag": "stringValue",
        "typeValueText": "CIE San Diego",
        "dataType": "String",
        "choiceText": "CIE San Diego",
        "name": "InitialRequestArea_CIE_SanDiego_Choice"
    })
    decisionRuleTag = create_tag(element_type="decision_rules", data={
        "name": "CIE_San_Diego",
        "conditionLogic": "and",
        "leftValueReference": "Field_InitialRequestArea_ChoiceField",
        "operator": "EqualTo",
        "rightValueElementReference": "InitialRequestArea_CIE_SanDiego_Choice",
        "label": "CIE San Diego",
    })
    choiceReferencesTag = create_tag("choiceReferences", data={
        "text": "InitialRequestArea_CIE_SanDiego_Choice"
    })

    # <choices> can be inserted at the top
    tree.getroot().insert(1, choiceTag)

    # <rules> should be inserted as the last of its element_type
    for decision in tree.findall(f"./{ns}decisions"):
        name = decision.find(f"{ns}name")
        if name.text == "Decision_on_InitialRequestArea":
            decision.append(decisionRuleTag)
            xml_printer(decision)
            break

    # <choiceReferences> should be inserted as the last of its element_type
    for screenfield in tree.findall(f"./{ns}screens//{ns}fields"):
        if screenfield.find(f"{ns}name").text == "Field_InitialRequestArea_ChoiceField":
            child_idx = -1
            insrt_idx = 0
            located = False
            for child in screenfield:
                child_idx += 1
                if child.tag == f"{ns}choiceReferences":
                    insrt_idx += 1
                    if not located:
                        located = True
                elif located:
                    screenfield.insert(child_idx, choiceReferencesTag)
                    break
            xml_printer(screenfield)
            break

    # save(tree)


if __name__ == "__main__":
    ET.register_namespace("", "http://soap.sforce.com/2006/04/metadata")

    tree = ET.parse("output/HelpDesk_MainFlow.flow-meta.xml")
    root = tree.getroot()
    ns = namespace(root)

    run(tree, root, ns)

    save(tree)
