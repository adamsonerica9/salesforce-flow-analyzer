import functools
import json
import xml.etree.ElementTree as ET

from sal_119 import (SIMPLE_ONCE_TAG_NAMES, create_tag, deduplicate, namespace,
                     tag_sorter, xml_printer)


# Insert new Lv1 choice
def general_service_feedback(root, ns, child_idx, label_and_value, choice_apiname, decisions, colx, rowx, incx, incy, first_branch_completed):
    root.insert(child_idx+1, create_tag(type="choices", data={
        "typeValueTag": "stringValue",
        "typeValueText": label_and_value,
        "dataType": "String",
        "choiceText": label_and_value,
        "name": choice_apiname
    }))

    # Insert new rule on Lv1 choice
    decisions.append(
        create_tag(type="decision_rules", data={
            "name": label_and_value.replace(" ", "_"),
            "conditionLogic": "and",
            "leftValueReference": "In_Lv1",
            "operator": "EqualTo",
            "rightValueElementReference": choice_apiname,
            "label": label_and_value,
            "targetReference_c": f"CaseDetails_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}"
        })
    )

    # Create screen fields
    account_name_field = create_tag(type="screen_field", data={
        "name": f"Field_AccountName_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}",
        "fieldType": "ComponentInstance",
        "isRequired": "true",
        "extensionName": "flowruntime:lookup",
        "inputsOnNextNavToAssocScrn": "UseStoredValues",
        "storeOutputAutomatically": "true",
        "inputParameters": [
            {
                "name": "fieldApiName",
                "typeValueTag": "stringValue",
                "typeValueText": "AccountId",
            },
            {
                "name": "label",
                "typeValueTag": "stringValue",
                "typeValueText": "What is the name of the agency?",
            },
            {
                "name": "objectApiName",
                "typeValueTag": "stringValue",
                "typeValueText": "Case",
            },
            {
                "name": "required",
                "typeValueTag": "booleanValue",
                "typeValueText": "true",
            },
        ]
    })
    subject_field = create_tag(type="screen_field", data={
        "name": f"Field_Subject_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}",
        "dataType": "String",
        "fieldText": "Subject",
        "fieldType": "InputField",
        "isRequired": "true",
    })
    descrip_field = create_tag(type="screen_field", data={
        "name": f"Field_Description_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}",
        "fieldText": "Description",
        "fieldType": "LargeTextArea",
        "isRequired": "true",
    })
    srvname_field = create_tag(type="screen_field", data={
        "name": f"Field_ServiceName_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}",
        "dataType": "String",
        "fieldText": "What is the name of the service?",
        "fieldType": "InputField",
        "isRequired": "true",
    })
    addinfo_field = create_tag(type="screen_field", data={
        "name": f"Field_AdditionalInfo_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}",
        "fieldText": "Any miscellaneous information that may be relevant to the request that was not adequately covered by other fields",
        "fieldType": "LargeTextArea",
        "isRequired": "false",
    })
    casedet_screen = create_tag(type="screens", data={
        "name": f"CaseDetails_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}",
        "label": "Case Details",
        "allowBack": "true",
        "allowFinish": "true",
        "allowPause": "true",
        "targetReference": f"CreateCase_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}",
        "showFooter": "true",
        "showHeader": "true",
        "x": str(colx),
        "y": str(rowx),
        "fields": [
            account_name_field, subject_field, descrip_field, srvname_field, addinfo_field
        ]
    })

    fault_screen = create_tag(type="fault_screen", data={
        "x": str(colx+incx),
        "y": str(rowx+incy)
    }) if not first_branch_completed else None

    createR_screen = create_tag(type="recordCreates", data={
        "name": f"CreateCase_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}",
        "label": "Create Case",
        "object": "Case",
        "storeOutputAutomatically": "true",
        "targetReference_c": f"FileUpload_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}",
        "targetReference_fc": "Fault_Screen",
        "isGoTo_fc": "true" if first_branch_completed else "false",
        "x": str(colx),
        "y": str(rowx+incy),
        "inputAssignments": [
            {
                "field": "AccountId",
                "elementReference": f"{account_name_field.find(f'name').text}.recordId"
            },
            {
                "field": "Additional_Information__c",
                "elementReference": addinfo_field.find(f"name").text
            },
            {
                "field": "Description",
                "elementReference": descrip_field.find(f"name").text
            },
            {
                "field": "Initial_Request_Area__c",
                "elementReference": "In_InitialRequestArea"
            },
            {
                "field": "Level_1_Category__c",
                "elementReference": "In_Lv1"
            },
            {
                "field": "RecordTypeId",
                "elementReference": "In_RecordType_ResourceDatabase.Id"
            },
            {
                "field": "OwnerId",
                "elementReference": "In_Queue_Resource.Id"
            },
            {
                "field": "Service_Name__c",
                "elementReference": srvname_field.find(f"name").text
            },
            {
                "field": "Subject",
                "elementReference": subject_field.find(f"name").text
            },
            {
                "field": "Support_Team__c",
                "stringValue": "Resource Center"
            },
        ],
    })

    fileUp_field = create_tag(type="screen_field", data={
        "name": f"Field_FileUpload_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}",
        "fieldType": "ComponentInstance",
        "isRequired": "true",
        "extensionName": "forceContent:fileUpload",
        "inputsOnNextNavToAssocScrn": "UseStoredValues",
        "storeOutputAutomatically": "true",
        "inputParameters": [
            {
                "name": "label",
                "typeValueTag": "stringValue",
                "typeValueText": "Attachments",
            },
            {
                "name": "recordId",
                "elementReference": createR_screen.find("name").text
            },
            {
                "name": "multiple",
                "typeValueTag": "booleanValue",
                "typeValueText": "true",
            },
            {
                "name": "disabled",
                "typeValueTag": "booleanValue",
                "typeValueText": "false",
            },
        ]
    })
    fileUp_screen = create_tag(type="screens", data={
        "name": createR_screen.find("./connector/targetReference").text,
        "label": "Attachments (Optional)",
        "allowBack": "true",
        "allowFinish": "true",
        "allowPause": "true",
        "showFooter": "true",
        "showHeader": "true",
        "x": str(colx),
        "y": str(rowx+incy*2),
        "fields": [
            fileUp_field
        ]
    })

    screen_idx = -1
    createR_inserted = False
    for child in root:
        screen_idx += 1
        if child.tag == f"{ns}processType":
            root.insert(screen_idx+1, createR_screen)
            break

    screen_idx = -1
    detected = False
    for child in root:
        screen_idx += 1
        if child.tag == f"recordCreates":
            detected = True
        if detected and child.tag != f"recordCreates":
            root.insert(screen_idx, casedet_screen)
            break

    screen_idx = -1
    detected = False
    for child in root:
        screen_idx += 1
        if child.tag == "screens" and "CaseDetails_when" in child.find("name").text:
            detected = True
        if detected and (child.tag != "screens" or child.tag == "screens" and "CaseDetails_when" not in child.find("name").text):
            root.insert(screen_idx, fileUp_screen)
            break

    if not first_branch_completed and fault_screen is not None:
        screen_idx = -1
        detected = False
        for child in root:
            screen_idx += 1
            if child.tag == "screens" and "FileUpload_when" in child.find("name").text:
                detected = True
            if detected and (child.tag != "screens" or child.tag == "screens" and "FileUpload_when" not in child.find("name").text):
                root.insert(screen_idx, fault_screen)
                break

def build_flow(layout: dict, flow: ET.Element):
    tree = ET.ElementTree(flow)
    root = tree.getroot()
    ns = namespace(root)

    layout_keys = list(layout.keys())
    
    for tag, tags in layout.items():
        if not len(tags):
            continue
        else:
            tags.sort(key=functools.cmp_to_key(tag_sorter))

        root = tree.getroot()

        tag_pos = layout_keys.index(tag)
        prev_tags = layout_keys[0:tag_pos+1]
        prev_tags.reverse()

        prev_sections_with_ns = {
            f'{ns}{tag}_exists': tree.find(f'./{ns}{tag}') is not None for tag in prev_tags
        }
        prev_sections_without_ns = {
            f'{tag}_exists': tree.find(f'./{tag}') is not None for tag in prev_tags
        }
        previous_sections = {**prev_sections_with_ns, **prev_sections_without_ns}
        # print(tag, json.dumps(previous_sections, indent=2))

        idx = -1
        detected = False
        first_time = True  # Whether the element being inserted is the first one into the skeleton base
        inserted = False
        for flag_name, prev_section_exists in previous_sections.items():
            if prev_section_exists:
                first_time = False
                tag_after_which_to_insert = flag_name.replace('_exists', '')

                for child in root:
                    idx += 1

                    if tag_after_which_to_insert in child.tag:
                        detected = True
                        # print(component, tags[0].tag)

                    if detected and tag_after_which_to_insert not in child.tag:
                        for i in range(len(tags)):
                            if tags[i] is not None:
                                root.insert(idx+i, tags[i])
                                # print(f'A - Insert {tags[i].tag} in ({idx}+{i})={idx+i}')
                        inserted = True
                        break
                break

        if first_time:
            for i in range(len(tags)):
                root.insert(i, tags[i])
                # print(f'B - Insert {tags[i].tag} in {i}')
        elif not first_time and not inserted:
            for i in range(len(tags)):
                root.insert(idx+i+1, tags[i])
                # print(f'C - Insert {tags[i].tag} in {idx+i+1}')
        

        # print(f'DEBUG: first time: {first_time}, inserted: {inserted}')

        deduplicate(tree)

        # print('Now Looks Like')
        # xml_printer(root)
        # print()

    
    return tree